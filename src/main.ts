import './assets/main.css'
import '@fontsource/source-sans-pro'
import '@fontsource/source-sans-pro/400.css'
import '@fontsource/source-sans-pro/700.css'
import "@fontsource/source-sans-pro/400-italic.css";

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(router)

app.mount('#app')
